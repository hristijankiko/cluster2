<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UsersController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        return 'index';
    }

    public function store(){

        return 'store';
    }

    public function create(){

        return 'create';
    }

    public function show(){

        return 'show';
    }

    public function update(){

        return 'update';
    }

    public function destroy(){

        return 'destroy';
    }

    public function edit(){

        return 'edit';
    }

}
